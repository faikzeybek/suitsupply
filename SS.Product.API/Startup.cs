﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SS.DataProvider;
using SS.Product.Repository;
using SS.Product.Repository.Repository;
using SS.Web.Middlewares.FirstLevelRequestHandler;
using SS.Web.Middlewares.HttpExceptionHandler;
using SS.Web.Middlewares.WebDispatcher;
using System;

namespace SS.Product.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(sp => { return new DataClient(Configuration["ConnectionStrings:ProductDatabase"]); });
            services.AddSingleton(sp => { return new CacheClient(Configuration["ConnectionStrings:CacheDatabase"]); });
            services.AddDbContext<ProductContext>(options =>
            {
                options.UseSqlServer(Configuration["ConnectionStrings:ProductDatabaseEF"],
                                     sqlServerOptionsAction: sqlOptions =>
                                     {
                                         //sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                                         sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                                     });
            });
            services.AddTransient(sp => { return new ProductRepository(sp.GetRequiredService<ProductContext>()); });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseHttpExceptionHandler();
            }
            else if (env.IsStaging())  // TODO --FAIK
            {
                app.UseHttpExceptionHandler();
                //app.UseExceptionHandler();
            }
            else  // production
            {
                app.UseHttpExceptionHandler();
                //app.UseExceptionHandler();
            }
            app.UseFirstLevelRequestHandler();
            //app.UseJwtAuthForUserApi();
            app.UseWebDispatcher();
        }
    }
}
