﻿using SS.Product.API.Model.Request;
using Entity = SS.Product.Repository.Entity;

namespace SS.Product.API.Model
{
    public static class Factory
    {
        public static Entity.Product ToDto(this AddProductRequest model)
        {
            return new Entity.Product
            {
                Name = model.Name,
                Price = model.Price,
                PhotoUri = model.PhotoUri
            };
        }

        public static Entity.Product ToDto(this UpdateProductRequest model)
        {
            return new Entity.Product
            {
                Id = model.Id,
                Name = model.Name,
                Price = model.Price,
                PhotoUri = model.PhotoUri,
                RowVersion = model.RowVersion
            };
        }
    }
}
