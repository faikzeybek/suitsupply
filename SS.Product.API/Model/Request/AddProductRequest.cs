﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SS.Product.API.Model.Request
{
    public class AddProductRequest
    {
        public string Name { get; set; }
        public string PhotoUri { get; set; }
        public decimal Price { get; set; }
    }
}
