﻿namespace SS.Product.API.Model.Request
{
    public class UpdateProductRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhotoUri { get; set; }
        public decimal Price { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
