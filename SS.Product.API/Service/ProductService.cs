﻿using Microsoft.Extensions.DependencyInjection;
using SS.DataProvider;
using SS.Product.API.Model;
using SS.Product.API.Model.Request;
using SS.Product.Repository.Repository;
using SS.Web.Middlewares.WebDispatcher;
using System;
using System.Threading.Tasks;

namespace SS.Product.API.Service
{
    public class ProductService
    {
        //  to add, edit, remove, view and search and export (Excel) product catalog items
        ICacheClient cacheClient;
        IDataClient dataClient;
        ProductRepository repository;
        public ProductService(IServiceProvider serviceProvider)
        {
            repository = serviceProvider.GetService<ProductRepository>();
        }

        public IServiceResult GETDetail(int id) // view
        {
            var result = repository.Get(id);

            return new ServiceResult { Data = result, StatusCode = 200 };
        }

        public IServiceResult GETAll() // view
        {
            var result = repository.GetAll();

            return new ServiceResult { Data = result, StatusCode = 200 };
        }

        public IServiceResult POST(AddProductRequest request) // add
        {
            repository.Add(request.ToDto());

            repository.UnitOfWork.Save();

            return new ServiceResult { StatusCode = 200 };
        }

        public IServiceResult POSTSearch(SearchRequest model) // search
        {
            return new ServiceResult { Data = "test data" };
        }

        //public IServiceResult POST(ExcelRequest model) // search
        //{
        //    return new ServiceResult { Data = "test data" };
        //}

        public IServiceResult PUT(UpdateProductRequest request) // edit
        {
            repository.Update(request.ToDto());

            repository.UnitOfWork.Save();

            return new ServiceResult { StatusCode = 200 };
        }

        public IServiceResult DELETE(int id) // remove
        {
            repository.Delete(id);

            repository.UnitOfWork.Save();

            return new ServiceResult { StatusCode = 200 };
        }

       
    }


}
