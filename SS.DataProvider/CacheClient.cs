﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;

namespace SS.DataProvider
{
    public class CacheClient : ICacheClient
    {
        private readonly IDatabase _db;
        private readonly ConnectionMultiplexer _redis;
        public CacheClient(string constr)
        {
            _redis = ConnectionMultiplexer.Connect(constr);
            _db = _redis.GetDatabase();
        }

        /// <summary>
        /// Remove value from redis
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            return _db.KeyDelete(key);
        }

        /// <summary>
        /// Remove multiple values from redis
        /// </summary>
        /// <param name="keys"></param>
        public void Remove(RedisKey[] keys)
        {
            _db.KeyDelete(keys);
        }

        /// <summary>
        /// Check if key is exist in redis
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            return _db.KeyExists(key);
        }

        /// <summary>
        /// Dispose DB connection
        /// </summary>
        //public void Stop()
        //{
        //    _db.Value.Dispose();
        //}

        /// <summary>
        /// Add new record in redis 
        /// </summary>
        /// <typeparam name="T">generic refrence type</typeparam>
        /// <param name="key">unique key of value</param>
        /// <param name="value">value of key of type T</param>
        /// <param name="expiresAt">time span of expiration</param>
        /// <returns>true or false</returns>
        public bool Add<T>(string key, T value, TimeSpan expiresAt) where T : class
        {
            var stringContent = JsonConvert.SerializeObject(value);
            return _db.StringSet(key, stringContent, expiresAt);
        }

        /// <summary>
        /// Add new record in redis 
        /// </summary>
        /// <typeparam name="T">generic refrence type</typeparam>
        /// <param name="key">unique key of value</param>
        /// <param name="value">value of key of type object</param>
        /// <param name="expiresAt">time span of expiration</param>
        /// <returns>true or false</returns>
        public bool Add(string key, object value, TimeSpan expiresAt)
        {
            var stringContent = JsonConvert.SerializeObject(value);
            return _db.StringSet(key, stringContent, expiresAt);
        }

        /// <summary>
        /// Add new record in redis 
        /// </summary>
        /// <typeparam name="T">generic refrence type</typeparam>
        /// <param name="key">unique key of value</param>
        /// <param name="value">value of key of type T</param>
        /// <returns>true or false</returns>
        public bool Update<T>(string key, T value) where T : class
        {
            var stringContent = JsonConvert.SerializeObject(value);
            return _db.StringSet(key, stringContent);
        }

        /// <summary>
        /// Get value of key, return one object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key) where T : class
        {
            try
            {
                RedisValue myString = _db.StringGet(key);
                if (myString.HasValue && !myString.IsNullOrEmpty)
                {
                    return JsonConvert.DeserializeObject<T>(myString);
                }

                return null;
            }
            catch (Exception)
            {
                // Log Exception
                return null;
            }
        }

    }
}
