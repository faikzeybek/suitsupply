﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SS.DataProvider
{
    public interface IDataClient
    {
        int ExecuteNonQuery(string queryText, object obj = null);
        IEnumerable<T> Query<T>(string queryText, object obj = null) where T : class;
    }
}
