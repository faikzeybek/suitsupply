﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace SS.DataProvider
{
    public interface ICacheClient
    {
        bool Add(string key, object value, TimeSpan expiresAt);
        bool Add<T>(string key, T value, TimeSpan expiresAt) where T : class;
        bool Exists(string key);
        T Get<T>(string key) where T : class;
        void Remove(RedisKey[] keys);
        bool Remove(string key);
        bool Update<T>(string key, T value) where T : class;
    }
}
