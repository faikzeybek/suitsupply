﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SS.DataProvider
{
    public class DataClient : IDataClient
    {
        readonly string _constr;
        public DataClient(string constr)
        {
            _constr = constr;
        }

        public int ExecuteNonQuery(string queryText, object obj = null)
        {
            using (var connection = new SqlConnection(_constr))
            {
                connection.Open();

                var transaction = connection.BeginTransaction();

                try
                {
                    int result = connection.Execute(queryText, obj, transaction);
                    transaction.Commit();

                    return result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                        connection.Close();

                }
            }
        }

        public IEnumerable<T> Query<T>(string queryText, object obj = null) where T : class
        {
            using (var connection = new SqlConnection(_constr))
            {
                try
                {
                    connection.Open();

                    IEnumerable<T> result = connection.Query<T>(queryText, obj);

                    return result;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
                }
            }
        }

    }
}
