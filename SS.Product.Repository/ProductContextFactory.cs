﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace SS.Product.Repository
{
    public class ProductContextFactory : IDesignTimeDbContextFactory<ProductContext>
    {
        public ProductContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProductContext>()
                .UseSqlServer("Server=.; Initial Catalog=SuitSupply.Product; Integrated Security=true");

            return new ProductContext(optionsBuilder.Options);
        }
    }
}
