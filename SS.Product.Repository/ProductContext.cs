﻿using Microsoft.EntityFrameworkCore;
using SS.Product.Repository.Configuration;
using SS.Product.Repository.Entity;
using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace SS.Product.Repository
{
    public class ProductContext : DbContext, IUnitOfWork
    {
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ProductEntityTypeConfiguration());
        }

        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
        }
        public DbSet<Entity.Product> Product { get; set; }

        public int Save()
        {
            try
            {
                OnBeforeSaving();
                var result = base.SaveChanges();
                return result;

            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw ex;
            }
        }

        void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Modified && entry.Entity is IBaseEntity)
                {
                    entry.State = EntityState.Modified;
                    ((IBaseEntity)entry.Entity).LastUpdated = DateTime.UtcNow;
                }
            }
        }
    }
}
