﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SS.Product.Repository.Entity
{
    public class BaseEntity : IBaseEntity
    {
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public DateTime LastUpdated { get; set; }

    }
}