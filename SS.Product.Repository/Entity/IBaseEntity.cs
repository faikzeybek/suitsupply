﻿using System;

namespace SS.Product.Repository.Entity
{
    public interface IBaseEntity
    {
        byte[] RowVersion { get; set; }
        DateTime LastUpdated { get; set; }

    }
}