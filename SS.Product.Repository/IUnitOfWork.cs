﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SS.Product.Repository
{
    public interface IUnitOfWork
    {
        int Save();
    }
}
