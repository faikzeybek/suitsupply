﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SS.Product.Repository.Configuration
{
    public class ProductEntityTypeConfiguration : IEntityTypeConfiguration<Entity.Product>
    {
        public void Configure(EntityTypeBuilder<Entity.Product> builder)
        {
            builder.ToTable("Product");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(p => p.Price)
                .IsRequired(true);

            builder.Property(p => p.PhotoUri)
               .IsRequired()
               .HasMaxLength(255);

            builder.Property(p => p.LastUpdated)
               .IsRequired();
        }
    }
}
