﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity = SS.Product.Repository.Entity;

namespace SS.Product.Repository.Repository
{
    public class ProductRepository
    {
        private readonly ProductContext _context;
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public ProductRepository(ProductContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IList<Entity.Product> GetAll()
        {
            var query = _context.Product;

            return query.ToList();
        }

        public void Add(Entity.Product model)
        {
            _context.Product.Add(model);
        }

        public void Update(Entity.Product model)
        {
            _context.Product.Update(model);
        }

        public void Delete(int id)
        {
            var result = _context.Product.FirstOrDefault(f => f.Id == id);

            if (result == null)
                throw new ArgumentNullException(nameof(result));

            _context.Product.Remove(result);
        }

        public Entity.Product Get(int id)
        {
            var result = _context.Product.FirstOrDefault(f => f.Id == id);

            if (result == null)
                throw new ArgumentNullException(nameof(result));

            return result;
        }
    }
}
