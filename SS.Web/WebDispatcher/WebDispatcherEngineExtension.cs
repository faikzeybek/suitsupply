﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Text;

namespace SS.Web.Middlewares.WebDispatcher
{
    public static class WebDispatcherExtension
    {
        public static IApplicationBuilder UseWebDispatcher(this IApplicationBuilder builder)
        { 
            return builder.UseMiddleware<WebDispatcherMiddleware>();
        }
    }
}
