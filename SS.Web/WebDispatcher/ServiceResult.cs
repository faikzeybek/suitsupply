﻿namespace SS.Web.Middlewares.WebDispatcher
{
    public class ServiceResult : IServiceResult
    {
        public int StatusCode { get; set; } = 200;
        public object Data { get; set; } = null;
        public string Message { get; set; } = "OK";
    }
}
