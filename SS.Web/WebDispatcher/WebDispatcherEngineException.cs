﻿using Newtonsoft.Json.Linq;
using System;

namespace SS.Web.Middlewares.WebDispatcher
{
    public class WebDispatcherException : Exception
    {
        public int StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";

        public WebDispatcherException(int statusCode)
        {
            StatusCode = statusCode;
        }

        public WebDispatcherException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public WebDispatcherException(int statusCode, Exception inner) : this(statusCode, inner.ToString())
        {
        }

        public WebDispatcherException(int statusCode, JObject errorObject) : this(statusCode, errorObject.ToString())
        {
            ContentType = @"application/json";
        }
    }
}
