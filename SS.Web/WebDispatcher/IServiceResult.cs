﻿namespace SS.Web.Middlewares.WebDispatcher
{
    public interface IServiceResult
    {
        object Data { get; set; }
        string Message { get; set; }
        int StatusCode { get; set; }
    }
}