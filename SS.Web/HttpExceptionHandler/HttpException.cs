﻿using Newtonsoft.Json.Linq;
using System;

namespace SS.Web.Middlewares.HttpExceptionHandler
{
    public class HttpException : Exception
    {
        public HttpException(int statusCode)
        {
            StatusCode = statusCode;
        }

        public HttpException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpException(int statusCode, Exception inner) : this(statusCode, inner.ToString())
        {
        }

        public HttpException(int statusCode, JObject errorObject) : this(statusCode, errorObject.ToString())
        {
            ContentType = @"application/json";
        }

        public int StatusCode { get; set; }

        public string ContentType { get; set; } = @"text/plain";
    }
}