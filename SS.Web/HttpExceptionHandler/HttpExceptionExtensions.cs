﻿using Microsoft.AspNetCore.Builder;

namespace SS.Web.Middlewares.HttpExceptionHandler
{
    public static class HttpExceptionHandlerExtensions
    {
        public static IApplicationBuilder UseHttpExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HttpExceptionHandlerMiddleware>();
        }
    }
}
