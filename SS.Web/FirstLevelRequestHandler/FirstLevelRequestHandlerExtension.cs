﻿using Microsoft.AspNetCore.Builder;

namespace SS.Web.Middlewares.FirstLevelRequestHandler
{
    public static class FirstLevelRequestHandlerExtension
    {
        public static IApplicationBuilder UseFirstLevelRequestHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<FirstLevelRequestHandlerMiddleware>();
        }
    }
}
