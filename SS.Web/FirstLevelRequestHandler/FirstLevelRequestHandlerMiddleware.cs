﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace SS.Web.Middlewares.FirstLevelRequestHandler
{
    public class FirstLevelRequestHandlerMiddleware
    {
        private readonly ILogger<FirstLevelRequestHandlerMiddleware> _logger;
        private readonly RequestDelegate _next;

        public FirstLevelRequestHandlerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory?.CreateLogger<FirstLevelRequestHandlerMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Invoke(HttpContext context)
        {
            //try
            //{
                if (context.Request.Path.HasValue && context.Request.Path.Value.Contains("favicon.ico"))
                {
                    context.Response.StatusCode = 404;

                    Console.WriteLine("Ignored!");

                    return;
                }

                await _next.Invoke(context);
            //}
            //catch (Exception ex)
            //{
            //    if (context.Response.HasStarted)
            //    {
            //        _logger.LogWarning(
            //            "The response has already started, the http status code middleware will not be executed.");
            //        throw;
            //    }

            //    context.Response.Clear();
            //    context.Response.StatusCode = 500;
            //    context.Response.ContentType = @"text/plain";

            //    await context.Response.WriteAsync(ex.Message);
            //}
        }
    }
}
