﻿using Newtonsoft.Json.Linq;
using System;

namespace SS.Web.Middlewares.FirstLevelRequestHandler
{
    public class FirstLevelRequestHandlerException : Exception
    {
        public int StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";

        public FirstLevelRequestHandlerException(int statusCode)
        {
            StatusCode = statusCode;
        }

        public FirstLevelRequestHandlerException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public FirstLevelRequestHandlerException(int statusCode, Exception inner) : this(statusCode, inner.ToString())
        {
        }

        public FirstLevelRequestHandlerException(int statusCode, JObject errorObject) : this(statusCode, errorObject.ToString())
        {
            ContentType = @"application/json";
        }
    }
}
