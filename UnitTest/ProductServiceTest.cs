using Microsoft.Extensions.DependencyInjection;
using SS.Product.API.Service;
using SS.Product.Repository.Repository;
using System;
using Xunit;

namespace UnitTest
{
    public class ProductServiceTest
    {
        readonly ProductDbFixture _fixture;
        readonly IServiceProvider _serviceProvider;

        public ProductServiceTest()
        {
            _fixture = new ProductDbFixture();

            var repository = new ProductRepository(_fixture.Context);

            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton(repository);
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        [Fact]
        public void Should_Return_200()
        {
            // arrange
            var service = new ProductService(_serviceProvider);

            // act
            var result = service.GETAll();

            // assert
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public void Should_Return_Same_Id()
        {
            // arrange
            var service = new ProductService(_serviceProvider);

            // act
            var result = service.GETDetail(2);
            var temp = (SS.Product.Repository.Entity.Product)result.Data;

            // assert
            Assert.True(temp.Id == 2);
        }

    }
}
