﻿using Microsoft.EntityFrameworkCore;
using SS.Product.Repository;
using System;
using System.Collections.Generic;
using Entity = SS.Product.Repository.Entity;

namespace UnitTest
{
    public class ProductDbFixture : IDisposable
    {
        public ProductContext Context { get; set; }

        public ProductDbFixture()
        {
            var builder = new DbContextOptionsBuilder<ProductContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            var options = builder.Options;
            Context = new ProductContext(options);
            Context.Database.EnsureCreated();

            this.InitDatabase();
        }

        private void InitDatabase()
        {
            var fakeClients = new List<Entity.Product>()
            {
                new Entity.Product { Id = 1, Name = "Apple", PhotoUri = "sampleAppleUri", Price = 2, LastUpdated = DateTime.Now },
                new Entity.Product { Id = 2, Name = "HP", PhotoUri = "sampleHPUri", Price = 9, LastUpdated = DateTime.Now },
                new Entity.Product { Id = 3, Name = "IBM", PhotoUri = "sampleIBMUri", Price = 1, LastUpdated = DateTime.Now },
                new Entity.Product { Id = 4, Name = "Microsoft", PhotoUri = "sampleMicrosoftUri", Price = 3, LastUpdated = DateTime.Now },
                new Entity.Product { Id = 5, Name = "Google", PhotoUri = "sampleGoogleUri", Price = 5, LastUpdated = DateTime.Now }
            };

            Context.Product.AddRange(fakeClients);

            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Database.EnsureDeleted();
            Context.Dispose();
        }
    }
}